package d_array_for;

/**
 * 【繰り返し】
 * 九九の7の段(7 ～ 63)をコンソールに表示してください。
 * ※配列は使用しません。
 */

public class Question04_03 {

	public static void main(String[] args) {
		int num = 7;
		for(int i = 1; i <= 9; i++) {
			int a = num * i;
			System.out.println(a);
		}

	}

}
